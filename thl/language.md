# Notations de ce document

\S : sigma majuscule
L1, L2 : languages
\C : contenu dans
\a : appartient à
PT : pour tout
O/ : ensemble vide
\e : epsilon

# Opérations sur les languages

PT L1 C \S\*, PT L2 C \S\*

```
L1 = { ab, bc }
L2 = { bc, cd }
```

### Concaténation :

```
L1 . L2 = { abbc, abcd, bcbc, bccd }
L1 . O/ = O/
L1 . { \e } = L1
```

### Puissance

PT n entier, PT u mot de \S\*, PT L1 C \S\*

```
u^n =
{
  n == 0 => \e
  n = 2 => u.u.u.u... n fois
}
L^n =
{
  n == 0 => { \e }
  n > 0 => L.L.L... n fois
}
```

### Préfixe

```
pref(abcaab) = { \e, a, ab, abc, abc, abcaa, abcaab }
```

### Étoile de Kleen

```
L* = Union de n = 0 à n = \inf de L^n = L^0 U L U L^2 U L^3...
L+ = L . L*
```

## Languages rationels

```
O/			.
{ \e }			U
PT a \a \S, { a }	*
```
Ces 6 éléments permettent de définir des languages rationnels, et tout les languages rationnels peuvent être définis en utilisant ces éléments.
