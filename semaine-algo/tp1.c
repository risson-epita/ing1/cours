#include <assert.h>
#include <stdio.h>

unsigned int int_width(int i)
{
    unsigned int width = 0;
    if (i <= 0)
        ++width;
    while (i != 0)
    {
        i /= 10;
        ++width;
    }
    return width;
}

unsigned int ints_width(const int *tab, unsigned int count)
{
    unsigned int king = int_width(tab[0]);
    for (unsigned int i = 1; i < count; ++i)
    {
        unsigned int current_int_width = int_width(tab[i]);
        if (current_int_width > king)
            king = current_int_width;
    }
    return king;
}

void print_int_array(FILE *out, const int *tab, unsigned int count)
{
    unsigned int width = ints_width(tab, count);
    unsigned int index_width = int_width(count - 1);
    int char_count = 0;
    for (unsigned int i = 0; i < count; ++i)
    {
        if (char_count == 0)
        {
            char_count += fprintf(out, "%*s[%u]", index_width - int_width(i), "", i);
        }
        if (char_count + width + 1 > 80)
        {
            fprintf(out, "\n");
            --i;
            char_count = 0;
            continue;
        }
        char_count += fprintf(out, " %*d", width, tab[i]);
    }
    fprintf(out, "\n");
}

void insert_sort(int* tab, unsigned count)
{
    for (unsigned int i = 1; i < count; ++i)
    {
        int key = tab[i];
        unsigned int j = i;
        for (; j > 0 && tab[j - 1] > key; --j)
            tab[j] = tab[j - 1];
        tab[j] = key;
    }
}

void insert_sort_cmp(int* tab, unsigned count, int (*cmp)(int a, int b))
{
    for (unsigned int i = 1; i < count; ++i)
    {
        int key = tab[i];
        unsigned int j = i;
        for (; j > 0 && cmp(tab[j - 1], key) == 1; --j)
            tab[j] = tab[j - 1];
        tab[j] = key;
    }
}
int main()
{
    return 0;
}
