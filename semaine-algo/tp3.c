#include <assert.h>
#include <stdbool.h>
#include <math.h>
#include <stdio.h>

unsigned int int_width(int i)
{
    unsigned int width = 0;
    if (i <= 0)
        ++width;
    while (i != 0)
    {
        i /= 10;
        ++width;
    }
    return width;
}

unsigned int ints_width(const int *tab, unsigned int count)
{
    unsigned int king = int_width(tab[0]);
    for (unsigned int i = 1; i < count; ++i)
    {
        unsigned int current_int_width = int_width(tab[i]);
        if (current_int_width > king)
            king = current_int_width;
    }
    return king;
}

void print_int_array(FILE *out, const int *tab, unsigned int count)
{
    unsigned int width = ints_width(tab, count);
    unsigned int index_width = int_width(count - 1);
    int char_count = 0;
    for (unsigned int i = 0; i < count; ++i)
    {
        if (char_count == 0)
        {
            char_count += fprintf(out, "%*s[%u]", index_width - int_width(i), "", i);
        }
        if (char_count + width + 1 > 80)
        {
            fprintf(out, "\n");
            --i;
            char_count = 0;
            continue;
        }
        char_count += fprintf(out, " %*d", width, tab[i]);
    }
    fprintf(out, "\n");
}

void swap(int *tab, unsigned i, unsigned j)
{
    int tmp = tab[i];
    tab[i] = tab[j];
    tab[j] = tmp;
}

void heapify(int *tab, unsigned i, unsigned n)
{
    unsigned largest = i;
    unsigned l = 2 * i + 1;
    unsigned r = 2 * i + 2;

    if (l < n && tab[l] > tab[largest])
        largest = l;

    if (r < n && tab[r] > tab[largest])
        largest = r;

    if (largest != i)
    {
        swap(tab, i, largest);
        heapify(tab, largest, n);
    }
}

void make_heap(int* tab, unsigned n)
{
    int startIdx = floor(n / 2) - 1;
    for (int i = startIdx; i >= 0; i--)
        heapify(tab, i, n);
}

bool is_heap(int *heap, unsigned i, unsigned n)
{
    if (i > (n - 2) / 2)
        return true;
    if (heap[i] >= heap[2*i + 1] && heap[i] >= heap[2*i + 2] &&
            is_heap(heap, 2*i + 1, n) && is_heap(heap, 2*i + 2, n))
        return true;
    return false;
}

bool check_heap(int* heap, unsigned n)
{
    return is_heap(heap, 0, n);
}

void heap_sort (int *a, int n)
{
    make_heap(a, n);
    for (int i = n - 1; i >= 1; --i)
    {
        swap(a, 0, i);
        heapify(a, 0, i);
    }
}

void pretty_print_heap(FILE* out, const int* v, unsigned n)
{
    int w = ints_width(v, n - 1);
    int count = 0;
    int nbline = 0;
    count++;
    for (unsigned j = 0; j < n; j = 2 * j + 1)
         nbline++;
    int plus = w + 1;
    int t = (plus) * pow(2, nbline - 1);
    int g = 0;
    for (unsigned i = 0; i < n; i = i)
    {
        int did = 0;
        int s = t / count - w;
        int to_floor = (s / 2);
        int nbspaces = floor(to_floor);
        for (int h = 0; h < nbspaces; h++)
            fprintf(out, " ");
        int test = g;
        for (int l = g; l < count + test && i < n; l++)
        {
            if (g != 0 && did == 1)
            {
                for (int k = 0; k < s; k++)
                    fprintf(out, " ");
            }
            for (int u = int_width(v[l]); u < w; u++)
                fprintf(out, " ");
            i++;
            g++;
            did = 1;
            fprintf(out,"%d" ,v[l]);
        }
        fprintf(out, "\n");
        count = count * 2;
    }
}

int pop_heap(int* heap, unsigned* n)
{
    int stk = heap[0];
    heap[0] = heap[*n - 1];
    *n = *n - 1;
    heapify(heap, 0, *n);
    return stk;
}

void heapify_cmp(int* v, unsigned i, unsigned n, int (*cmp)(int a, int b))
{
    unsigned l = 2 * i + 1;
    unsigned r = 2 * i + 2;
    unsigned g;
    if (l < n && cmp(v[l], v[i]) == 1)
        g = l;
    else
        g = i;
    if (r < n && cmp(v[r], v[g]) == 1)
        g = r;
    if (g != i)
    {
        int tmp = v[i];
        v[i] = v[g];
        v[g] = tmp;
        heapify_cmp(v, g, n, cmp);
    }

}

void make_heap_cmp(int* v, unsigned n, int (*cmp)(int a, int b))
{
    for (int i = floor(n / 2) - 1; i >= 0; i--)
        heapify_cmp(v, i, n, cmp);
}

bool check_heap_cmp(const int* v, unsigned n, int (*cmp)(int a, int b))
{
    if (n <= 1)
        return true;

    for (unsigned i = 0; i <= (n - 2) / 2; i++)
    {
        if (cmp(v[2 * i + 1], v[i]) == 1)
                return false;

        if (cmp(2 * i + 2, n) == 0 && cmp(v[2 * i + 2], v[i]) == 1)
                return false;
    }
    return true;

}

int pop_heap_cmp(int* heap, unsigned* n, int (*cmp)(int a, int b))
{
    int stk = heap[0];
    heap[0] = heap[*n];
    *n = *n - 1;
    heapify_cmp(heap, 0, *n, cmp);
    return stk;

}

void heap_sort_cmp(int* tab, unsigned n, int (*cmp)(int a, int b))
{
    make_heap_cmp(tab, n, cmp);
    for (int i = n - 1; i >= 1; i--)
    {
        int tmp = tab[0];
        tab[0] = tab[i];
        tab[i] = tmp;
        heapify_cmp(tab, 0, i, cmp);
    }
}

